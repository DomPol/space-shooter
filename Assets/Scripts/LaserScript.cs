﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    
    public Vector2 axis;
    
   
    void Update() {

       axis.x = Input.GetAxis ("Horizontal");
       axis.y = Input.GetAxis ("Vertical");   
       
       transform.Translate (axis * 5 * Time.deltaTime);   

       
        
    }

}
