﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    
    public float Speed;
    public Vector2 shipAxis;
    public Vector2 limits;
    public Weapon weapon;
    public float shootTime=0f;
    public Propeller prop;
    public AudioSource audioSource; 
    public ParticleSystem ps;
    public GameObject graphic;
    public Collider2D collider;

    public int lives = 3;
    private bool iamDead = false;

   
    void Update() {

        if(iamDead){
            return;
        }

       shootTime += Time.deltaTime; 
       transform.Translate (shipAxis * Speed * Time.deltaTime);       

        if (transform.position.y > limits.y)
        {
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
        }
        else if (transform.position.y < -limits.y)
        {
            transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
        }

        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -limits.x)
        {
            transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
        }
        if (shipAxis.x>0){

            prop.BlueFire();
        }

        
        if (shipAxis.x<0){

            prop.RedFire();
        }

        if (shipAxis.x == 0){
            prop.Stop();
        }
    }

    public void SetAxis(Vector2 currentAxis)
    {
        shipAxis = currentAxis;
    }

    public void SetAxis(float x, float y)
    {
        shipAxis = new Vector2(x,y);
    }

    public void Shoot (){
       if (shootTime>weapon.GetCadencia()){
           shootTime = 0f;
           weapon.Shoot();
       }
        
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Meteor"){
            StartCoroutine(DestroyShip());
    }
    }

    IEnumerator DestroyShip(){

        iamDead=true;

        lives--;

        graphic.gameObject.SetActive(false);

        collider.enabled = false;

        prop.gameObject.SetActive(false);
        
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(0.5f);

        //Me destruyo a mi mismo
        if(lives>0){
            StartCoroutine(inMortal());
        }
    }

    
    IEnumerator inMortal(){

        iamDead=false;
        
        graphic.SetActive(true);

        prop.gameObject.SetActive(true);
        
    

        //Lanzo sonido de explosion
        for (int i=0; i <15 ;i++){
            graphic.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            graphic.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        collider.enabled =true;

    }

}
