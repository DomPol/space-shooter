﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    private Vector2 speed = new Vector2(-1, 0);
    public GameObject Bullet;

    private void Start()
    {
        StartCoroutine(EnemyBehaviour());
    }

    void Update()
    {
         transform.Translate(speed * Time.deltaTime);
    }

    IEnumerator EnemyBehaviour()
    {
        while (true)
        {
            //Avanza horizontalmente
            speed.y= 1f;

            yield return new WaitForSeconds(1.0f);

            //Se para
            speed.y = -1f;

            yield return new WaitForSeconds(1.0f);

            //Dispara
            Instantiate (Bullet, transform.position, Quaternion.identity, null);
            Bullet.transform.Rotate (0,0,-100);
            
        }
    }
}
