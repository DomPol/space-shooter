﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour
{
    public Vector2 axis;

    public PlayerBehaviour player;
    void Update()
    {
        axis.x = Input.GetAxis ("Horizontal");
        axis.y = Input.GetAxis ("Vertical");

       

        if(Input.GetButton ("Fire1")){
            player.Shoot();
            
        }
        player.SetAxis(axis);
    }
}
