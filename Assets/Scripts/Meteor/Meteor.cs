﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    public GameObject [] graphics;
    int seleccionado;
    Vector2 speed;
    public AudioSource audioSource; 
    public ParticleSystem ps;
    public GameObject meteorToInstanciate;

    
  
    void Awake()
    {
        for (int x= 0; x<graphics.Length ; x++){
            graphics[x].SetActive(false) ;
        }

        seleccionado = Random.Range(0,graphics.Length);
        graphics[seleccionado].SetActive(true);

        speed.x = Random.Range(-4,0);          
        speed.y = Random.Range(-4,4);
    }
    
    void Update(){
        transform.Translate(speed*Time.deltaTime);
        graphics[seleccionado].transform.Rotate(0,0,100*Time.deltaTime);
    }

 

     public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy(this.gameObject);
        } else if(other.tag == "Bullet") {
            StartCoroutine(DestroyMeteor());
        }
    }

    IEnumerator DestroyMeteor(){
        //Desactivo el grafico
        graphics[seleccionado].gameObject.SetActive(false);

        //Destruyo el box collider

        Destroy(GetComponent<BoxCollider2D>());

        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        InstanceMeteor();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);

        
    }

    public virtual void InstanceMeteor(){
        Instantiate (meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate (meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate (meteorToInstanciate, this.transform.position, Quaternion.identity, null);
        Instantiate (meteorToInstanciate, this.transform.position, Quaternion.identity, null);
    }

   
   
}
