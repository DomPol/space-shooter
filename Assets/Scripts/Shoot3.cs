﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot3 : MonoBehaviour
{
   public float speed;
   public Vector2 direction;
   private float counter;
   
   

    void Update()
    {
       
       transform.Translate(direction * speed * Time.deltaTime);
       
       counter++;
       
      
       if (counter == 60){
           direction.y = -direction.y;
       }
       if (counter == 120){
           direction.y = -direction.y;
           counter =0;
       }
      
       
    }



    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish") {
            Destroy (gameObject);
        }
    }
    
}