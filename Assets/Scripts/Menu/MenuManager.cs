﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuManager : MonoBehaviour
{
    public void PulsarPlay(){
        SceneManager.LoadScene("Game");

    }

    public void PulsarCredits(){
        SceneManager.LoadScene("Credits");

    }

    public void PulsarMenu(){
        SceneManager.LoadScene("MainMenu");

    }

    public void PulsarExit(){
        Application.Quit();
    }
}
