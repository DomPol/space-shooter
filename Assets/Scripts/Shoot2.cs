﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot2 : MonoBehaviour
{
   public float speed;
   public Vector2 direction;
   private float counter;
   
   

    void Update()
    {
       
       transform.Translate(direction * speed * Time.deltaTime);
       
       counter++;
       
       if (counter == 30){
           direction.x =0;
           direction.y =1;
       }
       if (counter == 60){
           direction.x =1;
           direction.y =0;
       }
       if (counter == 90){
           direction.x =0;
           direction.y =-1;
       }
       if (counter == 120){
           direction.x =1;
           direction.y =0;
           counter=0;
       }
       
    }



    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish") {
            Destroy (gameObject);
        }
    }
    
}